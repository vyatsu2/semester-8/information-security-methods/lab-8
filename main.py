# coding=utf-8
# Импортируем библиотеку cryptography
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import padding

# Генерируем параметры для схемы Диффи-Хеллмана
parameters = dh.generate_parameters(generator=2, key_size=2048)

# Генерируем пары ключей для Алисы и Боба
alice_private_key = parameters.generate_private_key()
alice_public_key = alice_private_key.public_key()
bob_private_key = parameters.generate_private_key()
bob_public_key = bob_private_key.public_key()

# Генерируем пары ключей для RSA для Алисы и Боба
alice_rsa_private_key = rsa.generate_private_key(public_exponent=65537, key_size=2048)
alice_rsa_public_key = alice_rsa_private_key.public_key()
bob_rsa_private_key = rsa.generate_private_key(public_exponent=65537, key_size=2048)
bob_rsa_public_key = bob_rsa_private_key.public_key()

# Сериализуем открытые ключи в формате PEM
alice_dh_pem = alice_public_key.public_bytes(encoding=serialization.Encoding.PEM,
                                             format=serialization.PublicFormat.SubjectPublicKeyInfo)
alice_rsa_pem = alice_rsa_public_key.public_bytes(encoding=serialization.Encoding.PEM,
                                                  format=serialization.PublicFormat.SubjectPublicKeyInfo)
bob_dh_pem = bob_public_key.public_bytes(encoding=serialization.Encoding.PEM,
                                         format=serialization.PublicFormat.SubjectPublicKeyInfo)
bob_rsa_pem = bob_rsa_public_key.public_bytes(encoding=serialization.Encoding.PEM,
                                              format=serialization.PublicFormat.SubjectPublicKeyInfo)

# Алиса и Боб обмениваются своими открытыми ключами по незащищенному каналу связи
# Здесь мы просто присваиваем переменные для удобства

alice_received_dh_pem = bob_dh_pem # Алиса получает открытый ключ Диффи-Хеллмана от Боба
alice_received_rsa_pem = bob_rsa_pem # Алиса получает открытый ключ RSA от Боба

bob_received_dh_pem = alice_dh_pem # Боб получает открытый ключ Диффи-Хеллмана от Алисы
bob_received_rsa_pem = alice_rsa_pem # Боб получает открытый ключ RSA от Алисы

# Алиса и Боб десериализуют полученные открытые ключи из формата PEM

alice_received_dh_pubkey = serialization.load_pem_public_key(alice_received_dh_pem)
alice_received_rsa_pubkey = serialization.load_pem_public_key(alice_received_rsa_pem)

bob_received_dh_pubkey = serialization.load_pem_public_key(bob_received_dh_pem)
bob_received_rsa_pubkey = serialization.load_pem_public_key(bob_received_rsa_pem)

# Алиса и Боб вычисляют общий секретный ключ по схеме Диффи-Хеллмана

alice_shared_secret = alice_private_key.exchange(alice_received_dh_pubkey)
bob_shared_secret = bob_private_key.exchange(bob_received_dh_pubkey)

# Проверяем, что общий секретный ключ одинаковый у обоих сторон

assert alice_shared_secret == bob_shared_secret

print("Общий секретный ключ:")
print(alice_shared_secret.hex())

# Алиса и Боб вычисляют хеш своих открытых ключей Диффи-Хеллмана
# Алиса и Боб вычисляют хеш своих открытых ключей Диффи-Хеллмана

alice_dh_hash = hashes.Hash(hashes.SHA256())
alice_dh_hash.update(alice_dh_pem)
alice_dh_digest = alice_dh_hash.finalize()

bob_dh_hash = hashes.Hash(hashes.SHA256())
bob_dh_hash.update(bob_dh_pem)
bob_dh_digest = bob_dh_hash.finalize()

# Алиса и Боб подписывают свои хеши открытых ключей Диффи-Хеллмана своими закрытыми ключами RSA

alice_signature = alice_rsa_private_key.sign(alice_dh_digest,
                                             padding.PSS(mgf=padding.MGF1(hashes.SHA256()),
                                                         salt_length=padding.PSS.MAX_LENGTH),
                                             hashes.SHA256())

bob_signature = bob_rsa_private_key.sign(bob_dh_digest,
                                         padding.PSS(mgf=padding.MGF1(hashes.SHA256()),
                                                     salt_length=padding.PSS.MAX_LENGTH),
                                         hashes.SHA256())

# Алиса и Боб обмениваются своими подписями по незащищенному каналу связи
# Здесь мы просто присваиваем переменные для удобства

alice_received_signature = bob_signature # Алиса получает подпись от Боба
bob_received_signature = alice_signature # Боб получает подпись от Алисы

# Алиса и Боб проверяют подписи с помощью полученных открытых ключей RSA

try:
    alice_received_rsa_pubkey.verify(alice_received_signature,
                                     alice_received_dh_pem,
                                     padding.PSS(mgf=padding.MGF1(hashes.SHA256()),
                                                 salt_length=padding.PSS.MAX_LENGTH),
                                     hashes.SHA256())
    print("Алиса успешно проверила подпись Боба")
except:
    print("Алиса не смогла проверить подпись Боба")

try:
    bob_received_rsa_pubkey.verify(bob_received_signature,
                                   bob_received_dh_pem,
                                   padding.PSS(mgf=padding.MGF1(hashes.SHA256()),
                                               salt_length=padding.PSS.MAX_LENGTH),
                                   hashes.SHA256())
    print("Боб успешно проверил подпись Алисы")
except:
    print("Боб не смог проверить подпись Алисы")

# Если все проверки прошли успешно, то Алиса и Боб могут использовать общий секретный ключ для шифрования дальнейшего обмена