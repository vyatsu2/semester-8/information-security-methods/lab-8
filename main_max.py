import random


# Генерация публичного ключа
def generate_public_key(p, g, private_key):
    public_key = (g ** private_key) % p
    return public_key


# Генерация общего секрета
def generate_shared_secret(p, their_public_key, my_private_key):
    shared_secret = (their_public_key ** my_private_key) % p
    return shared_secret


# Шифрование сообщения при помощи общего секрета
def encrypt(message, shared_secret):
    encrypted_message = []
    for i in range(len(message)):
        encrypted_message.append(ord(message[i]) ^ shared_secret)
    return encrypted_message


# Расшифровка сообщения при помощи общего секрета
def decrypt(encrypted_message, shared_secret):
    decrypted_message = []
    for i in range(len(encrypted_message)):
        decrypted_message.append(chr(encrypted_message[i] ^ shared_secret))
    return ''.join(decrypted_message)


if __name__ == '__main__':
    # Пример использования
    p = 23
    g = 5

    # Генерация закрытых ключей для Алисы и Боба
    alice_private_key = random.randint(1, p - 1)
    print(f"Приватный ключ Алисы: {alice_private_key}")
    # bob_private_key = random.randint(1, p - 1)
    # print(f"Приватный ключ Bob: {bob_private_key}")

    # Генерация открытых ключей для Алисы и Боба
    alice_public_key = generate_public_key(p, g, alice_private_key)
    print(f"Публичный ключ Алисы: {alice_public_key}")
    bob_public_key = int(input("Полученный публичный ключ Боба: ")) #generate_public_key(p, g, bob_private_key)

    # Генерация общего секрета для Алисы и Боба
    shared_secret_alice = generate_shared_secret(p, bob_public_key, alice_private_key)
    # shared_secret_bob = generate_shared_secret(p, alice_public_key, bob_private_key)

    # Шифрование и расшифровка с использованием общего секрета
    message = "Д. Ю: 'Поздравляю с 8 марта!'"
    encrypted_message = encrypt(message, shared_secret_alice)
    # decrypted_message = decrypt(encrypted_message, shared_secret_bob)

    print(f'Общий секрет: {shared_secret_alice}')
    print("Исходное сообщение:", message)
    print("Зашифрованное сообщение:", encrypted_message)
    # print("Расшифрованное сообщение:", decrypted_message)
